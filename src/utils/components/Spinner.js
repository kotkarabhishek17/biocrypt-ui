import React from 'react'

export default function Spinner() {
    return (
        <span className="spinner-border spinner-border-sm"></span>
    );
}
