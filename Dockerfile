FROM node:12.16

RUN mkdir /app
RUN mkdir /app/ui
WORKDIR /app/ui

COPY ./package.json /app/ui/

RUN npm cache clean --force
RUN npm install

COPY . /app/ui/

EXPOSE 3000
